\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{minted}
\usepackage[dvipsnames]{xcolor}
\usepackage{fancyhdr}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[margin=1.2in,letterpaper]{geometry}
\usepackage{todonotes}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage{needspace}
\usepackage{tabto}
\usepackage{fontawesome}
\usepackage{xcolor}
\usepackage{pdfpages}
\usepackage{ragged2e}
\usepackage{parcolumns}

\DeclareUnicodeCharacter{251C}{\mbox{\kern.23em
  \vrule height2.2exdepth1exwidth.4pt\vrule height2.2ptdepth-1.8ptwidth.23em}}
\DeclareUnicodeCharacter{2502}{\mbox{\kern.23em
  \vrule height2.2exdepth1exwidth.4pt}}
\DeclareUnicodeCharacter{2500}{\mbox{\vrule height2.2ptdepth-1.8ptwidth.5em}}
\DeclareUnicodeCharacter{2514}{\mbox{\kern.23em
  \vrule height2.2exdepth-1.8ptwidth.4pt\vrule height2.2ptdepth-1.8ptwidth.23em}}

\setminted[console]{
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\scriptsize,
    breaklines,
    breakanywhere,
    frame=single
}

\setminted[bash]{
    fontsize=\scriptsize,
    breaklines,
    breakanywhere,
    linenos,
    numberblanklines,
    breaklines,
    frame=single
}

\setminted[cpp]{
    fontsize=\scriptsize,
    breaklines,
    breakanywhere,
    linenos,
    numberblanklines,
    breaklines,
    frame=single
}

%   Minted insertion options:
%   \inputminted[frame=single]{cpp}{files/ex1/main1.c}
%\inputminted{console}{files/ex1/main1_valgrind}

\fancyhead[R]{Machine Learning Basics}
\makeatletter
\fancyhead[L]{Adam True}
\makeatother
\fancyfoot[C]{\thepage}

\title{\Huge Machine Learning Basics \\\Large Introduction to Supervised Learning}
\author{Adam True}

\date{}

% Background colour for the code lines
\definecolor{bg}{rgb}{0.9,0.9,0.9}
\definecolor{apple1}{RGB}{239, 16, 2}
\definecolor{apple2}{RGB}{174, 26, 0}
\definecolor{orange1}{RGB}{254, 145, 6}
\definecolor{orange2}{RGB}{255, 106, 0}
\definecolor{orange3}{RGB}{255, 134, 48}
\definecolor{banana}{RGB}{244, 235, 65}

\begin{document}

\clearpage\maketitle

\vspace{2.5cm}

\centering
\includegraphics[scale=0.33]{images/cover_draft1.png}

\vspace{2.5cm}

\includegraphics[scale=0.35]{images/Hesso_logo.png}

\raggedright
\thispagestyle{empty}

\newpage


\newpage
\justify
This document serves to shed a light on some of the fundamental elements of machine learning, which tools to use, how to start and where to go from there. 

\begin{itemize}
    \item[\Large \faGitSquare] Git repository : \url{https://bitbucket.org/adamalpaca/fruit-recognition/src/master/}
    
\end{itemize}{}

\section{Useful Tools}

\subsection{Software development}
\begin{itemize}
    \item Python 2 or 3 : programming language
    \item Colaboratory : Google provided Jupyter notebook environment for running programs in the cloud
\end{itemize}

\subsection{Python libraries}
\begin{itemize}
    \item \textbf{cv2} : OpenCV, used for image feature extraction, such as finding contours
    \item \textbf{pandas} : python data analysis library
    \item \textbf{numpy} : NumPy, scientific computing, mainly used for its N-dimensional array objects in this project
    \item \textbf{skimage} : read data from jpg into a masked float array
    \item \textbf{matplotlib} : graph plotting library
    \item \textbf{sklearn} : library for machine learning
\end{itemize}

\newpage
\section{Vocabulary}

\begin{itemize}
    \item[] \textbf{data set} (or dataset): it concerns the total collection of data that will be used to train and test an algorithm
    \item[] \textbf{class}: a class is a distinct structure that the algorithm is trained to recognise. For example if an algorithm is trained to recognise different fruits, the classes can be `apple', `orange', `strawberry', `banana', ... 
    \item[] \textbf{feature}: a feature is a calculable or distinguishable characteristic of a class or sample. This could be the colour of a fruit, the shape or the curvature, etc. 
    \item[] \textbf{sample}: a sample is one instance of a class and its listed features. For example, here is a list of samples with one feature (colour):
    
    % ------- This is the minipage containing sample examples
    \centering
    \vspace{0.3cm}
    \colorbox{bg}{\begin{minipage}[bgcolor=bg]{0.6\textwidth}
    \textit{Index} \hspace{1cm} \textit{Class} \tab \textit{Colour}
    \begin{enumerate}
        \item \hspace{1cm} `Apple' \tab rgb(239, 16, 2) \hspace{0.59cm}\textcolor{apple1}{\Large\faApple}
        \item \hspace{1cm} `Orange' \tab rgb(254, 145, 6)\hspace{0.55cm}\textcolor{orange1}{\Large\faCircle}
        \item \hspace{1cm} `Apple' \tab rgb(175, 26, 0)\hspace{0.745cm}\textcolor{apple2}{\Large\faApple}
        \item \hspace{1cm} `Banana' \tab rgb(244, 235, 65)\hspace{0.45cm}\textcolor{banana}{\Large\faCircle}
        \item \hspace{1cm} `Orange' \tab rgb(255, 106, 0)\hspace{0.6cm}\textcolor{orange2}{\Large\faCircle}
        \item \hspace{1cm} `Orange' \tab rgb(255, 134, 48)\hspace{0.44cm}\textcolor{orange3}{\Large\faCircle}
    \end{enumerate}
    \end{minipage}}
    % -----------------------------------------------
    
    \justify
    
    \item \textbf{labelled data}: data unit that has been given a class type. This is generally done by humans. Typically, labelled data can be an image of a frog with the label `frog'
    \item \textbf{folding}: process of splitting a labelled data set into groups in order to have data to train the algorithm and data to test it
    \item \textbf{classifier} : type of algorithm used to determine the class of a sample. Examples are \textbf{kNN} - k-nearest neighbours, neural networks, ...
    \item \textbf{training}: the algorithm using data to set its parameters 
    \item \textbf{testing}: using the rest of the data to check whether the algorithm has performed a good job
\end{itemize}

\newpage
\begin{figure}[!h]
\begin{minipage}[!h]{0.6\textwidth}
\section{Process}

\subsection{Set-up}
In this document, the process for machine learning is illustrated with the following example: training an algorithm to distinguish between apples, oranges and bananas. The data is taken from Fruits-360 

(\url{https://github.com/Horea94/Fruit-Images-Dataset}). 

How I extracted data and compiled a \texttt{zip} file of the neccessary data can be found in Appendix \ref{app:extract}.

The \texttt{Jupyter} file for this project is \texttt{fruit\_recognition.ipynb}, in Appendix \ref{app:jupyter}. 

\subsection{Classifier}
We will be using a \textbf{kNN classifier} (\url{https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm}). This is basically where the algorithm plots data points for each sample in a graph where the axes are its features. Then when the features are extracted from a sample to test, kNN calculates this point's nearest \textbf{n} neighbours (Euclidean distance) and whichever class appears the most is taken as the result.  An explanatory diagram can be seen in figure \ref{fig:knn}.

To simplify the project I have decided to assemble everything into one Python class (the methods in this section are methods of this class):

\begin{minted}[bgcolor=bg]{python}
class KNN:
  def __init__(self):
    self.X = []
    self.X_reduce = []
    self.classnames = []
    self.y = []
    self.Xtrain = []
    self.Xtest = []
    self.ytrain = []
    self.ytest = []
    self.skf = None
    self.classifier = None
    self.PCA = None
\end{minted}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}[!h]{0.3\textwidth}
%  \begin{figure}[!h]%{R}{0.3\textwidth}
%
    \centering
    \includegraphics[width=0.8\linewidth]{images/xfig}
    \caption{Representation of X}
    \label{fig:X_fig}
    
    \vspace{2.7cm}

    \includegraphics[width=0.8\linewidth]{images/knn}
   \caption{kNN - the input is identified as class 1, where n=5}
    \label{fig:knn}
%\end{figure}
\end{minipage}
%\Needspace{10pt}%% Same as image height 
\end{figure}

\newpage
\justify
\subsection{Load Data}
First of all your files should be organised in the following way:

\vspace{0.5cm}

\centering
\begin{minipage}[!h]{0.7\textwidth}
\begin{minted}{console}
C:.
│
├───Data
│   │   meta.pkl
│   │
│   └───Images
│           apple0_100.jpg
│           apple100_100.jpg
│           apple101_100.jpg
│           apple102_100.jpg
│           apple103_100.jpg
...
\end{minted}
\end{minipage}

\vspace{0.5cm}

\justify
The data frame is stored in a \texttt{pkl} file. This is a serialisation method used in Python. The object loaded from this file is a \texttt{pandas} data frame, resembling the following:

\begin{table}[!h]
\begin{tabular}{lll}
  & \textbf{filename}   & \textbf{classid} \\
\textbf{0} & banana0\_100.jpg   & banana  \\
\textbf{1} & banana104\_100.jpg & banana  \\
\textbf{2} & banana107\_100.jpg & banana  \\
\textbf{3} & banana10\_100.jpg  & banana  \\
\textbf{4} & banana112\_100.jpg & banana  \\
$\cdots$ & $\cdots$ & $\cdots$ \\
\textbf{1921} &  oranger\_73\_100.jpg &  orange \\
\textbf{1922} &  oranger\_74\_100.jpg &  orange \\
\textbf{1923} &  oranger\_75\_100.jpg &  orange \\
\textbf{1924} &  oranger\_76\_100.jpg &  orange
\end{tabular}
\end{table}

\subsection{Feature extraction}
In this document we are only looking at feature \textit{extraction} and not at feature \textit{construction}, which is also a possibility. Where we basically create new features through a combination of methods on other features.

For the fruit project, we could use a whole image as a feature. This would mean having 30'000 features (100x100 pixels for 3 colours), which is not very optimal. Instead we merely use the colour histogram for red, green and blue channels; reducing the amount of features down to 768. 

The features could, however, include many things such as frequency for a time-series, standard deviations, averages, ...

\newpage
\subsection{Class vector}
To start off we create a vector \textbf{y}, this will contain the class types of every sample in our data set in order. In order to more easily distinguish the class names, we transform them into integers:
\begin{itemize}
    \item `banana' $\Rightarrow$ 0
    \item `apple' $\Rightarrow$ 1
    \item `orange' $\Rightarrow$ 2
\end{itemize}

We do this with the \texttt{preprocessing} library from \texttt{sklearn}, and using a \texttt{LabelEncoder}.

\begin{minted}[bgcolor=bg]{python}
from sklearn import preprocessing
le = preprocessing.LabelEncoder()

def build_class_vector(self):
    for idx in range(0, len(meta)):
      # Create a vector of the class names
      self.classnames.append(meta.iloc[idx]['classid'])
    
    # Associate class names with integers
    le.fit(self.classnames)
    self.y = le.transform(self.classnames)
\end{minted}

\subsection{Matrix construction}
A matrix \textbf{X} will be created that is essentially a list of samples and their features, it is in \textit{exactly} the same order as \textbf{y}. This resembles the image in figure \ref{fig:X_fig}.

$$ X = 
 \begin{bmatrix}
  f_{11} & f_{12} & \cdots & f_{1n} \\ 
  f_{21} & f_{22} & \cdots & f_{2n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  f_{m1} & f_{m2} & \cdots & f_{mn} 
 \end{bmatrix} $$ 
 
 Where $f_{jk}$ is feature number j of sample number k. 

\begin{minted}[bgcolor=bg]{python}
self.X = []
# For each image
for idx in range(0,len(meta)): 
    # Feature vector for this image
    fv = []

    # Load this image
    img = load_img(meta.iloc[idx]['basename'])

    # Add features to feature vector
    hist = colour_histogram(img, plot=False)
    for k in range (0, len(hist)):
        for kk in range (0, len(hist[k])):
            fv.append(int(hist[k][kk]))
          
    # Add to matrix
    self.X.append(fv)
    
    #continued later (1)
\end{minted}

\subsection{Feature Reduction}
In this part we will reduce our initial number of features. There are many methods for doing this, but the one analysed here will be \textbf{P}rincipal \textbf{C}omponent \textbf{A}nalysis or \textbf{PCA} (see \url{https://en.wikipedia.org/wiki/Principal_component_analysis}).

The ``simple" way of explaining this process is this: the algorithm detects which features are more important for determining class types by looking at the variance of each feature in coordination with the class. It then extracts a quantity of these important features, a number which we choose.

Basically if there is a feature which has the same value for every class, it won't be selected as it can't help us determine anything. If there is a feature that is random noise for every class, it shouldn't be selected either. 

As an example, let's invent a feature that helps us easily distinguish classes:

\textbf{orangeness}

Let's say \textbf{orangeness} is 100\% for every orange, 50\% for every banana and 0\% for every apple (These numbers are just selected by me and it means nothing). Then the \textbf{PCA} should be able to determine it as an important feature, possibly the most important one.

Voilà, so the idea is to reduce the amount of features and hence the learning time for the classifier:

\begin{minted}[bgcolor=bg]{python}
# This is the continuation of method from before (1)
minmax = preprocessing.MinMaxScaler()
from sklearn.decomposition import PCA

self.X = minmax.fit_transform(self.X)
self.pca = PCA(n_components=components).fit(self.X)
self.X_reduce = self.pca.transform(self.X)
\end{minted}

We use a mechanism from \texttt{sklearn.preprocessing} called \texttt{MinMaxScaler}. This allows us to ``normalise" every feature. This way there is no bias towards features with naturally higher or lower values than the others. This is neccessary for \textbf{kNN} because we calculate the Euclidean distance between points.

\subsection{Folding the data}
We are now going to ``fold" the data into 2 parts: training data and test data. For this we use the tool \texttt{StratifiedKFold} in the library \texttt{sklearn.model\_selection}. The reason we use \texttt{StratifiedKFold} and not a simple \texttt{KFold}, is because the stratified version attempts to equally seperate each class type.

\begin{minted}[bgcolor=bg]{python}
from sklearn.model_selection import StratifiedKFold

def split_data(self):
    self.skf = StratifiedKFold(2, False, None)
    Xtrain_indexes, Xtest_indexes = self.skf.split(self.X_reduce, self.y)

    # Create vectors for training
    for i in range (0, len(Xtrain_indexes[0])):
      self.Xtrain.append(self.X_reduce[Xtrain_indexes[0][i]])
      self.ytrain.append(self.y[Xtrain_indexes[0][i]])

    # Create vectors for testing
    for i in range (0, len(Xtest_indexes[0])):
      self.Xtest.append(self.X_reduce[Xtest_indexes[0][i]])
      self.ytest.append(self.y[Xtest_indexes[0][i]])
\end{minted}

\subsection{Learning}
Now that we have two data sets, we can use one of the data sets to train the algorithm and the other one to test if the algorithm works:

\begin{minted}[bgcolor=bg]{python}
from sklearn.neighbors import KNeighborsClassifier

def train(self, neighbours):
    # Create a classifier for the training
    self.classifier = KNeighborsClassifier(n_jobs=-1,n_neighbors=neighbours,
    weights='uniform')
    # Train algorithm with training set
    self.classifier.fit(self.Xtrain, self.ytrain)
\end{minted}

\subsection{Summary}
Let's bring it all together in a nice sequence:
\newcommand{\myPoint}{\faCircleONotch}
\begin{itemize}
    \item[\myPoint] Create class vector \textbf{y}
    \item[\myPoint] Select and extract features
    \item[\myPoint] Create sample matrix \textbf{X}
    \item[\myPoint] Fold data set into $X_{train}$, $X_{test}$, $y_{train}$, $y_{test}$
    \item[\myPoint] Execute feature reduction techniques (PCA, other or hybrid)
    \item[\myPoint] Train the classifier
    \item[\myPoint] Create predicted class type vector $y_{pred}$ using the training set
    \item[\myPoint] Compare $y_{pred}$ with $y_{test}$ to determine the accuracy of the algorithm
    \item[\faRepeat] If the accuracy is low, then change the parameters of the different methods and start again
    \item[\faCoffee] When it's all good, sit down with a well deserved beverage 
    
\end{itemize}
\section{Results}
The results of training on our data set show a 100\% precision. Here is the confusion matrix and report:

\includepdf[page={15}]{files/fruit_recognition.pdf}

\newpage
\section{Appendices}

\subsection{Data Extraction} \label{app:extract}

\begin{minted}[bgcolor=bg]{python}
import os
from shutil import copyfile
import pandas as pd

fruit_root = 'C:\\Users\\adam.true\\Desktop\\Fruit-Images-Dataset'
work_root = 'C:\\Users\\adam.true\\Desktop\\Work\\Amara_MachineLearning'

def get_files_in_path(mypath):
    f = []
    for (dirpath, dirnames, filenames) in os.walk(mypath):
        f.extend(filenames)
        break
    return f

def copy_file_to_new_location(oldFP, newFP):
    copyfile(oldFP, newFP)

def copy_all_files_to_new_location(oldFP, newFP, nameOfFruit, file_data):
    # Get all of the file names within the directory
    files_to_copy = get_files_in_path(oldFP)
    # We will copy the new file names into this data array along with the name 
    # of the fruit
    for f in files_to_copy:
        copy_file_to_new_location(oldFP+'\\'+f, newFP+'\\'+nameOfFruit+f)
        file_data.append([nameOfFruit+f, nameOfFruit])
    return file_data



data = []

# Get banana files
copy_all_files_to_new_location(fruit_root +'\\Training\\Banana', work_root
+'\\Data\\Images','banana', data)

copy_all_files_to_new_location(fruit_root +'\\Test\\Banana', work_root
+'\\Data\\Images','banana', data)

# Get some apple files
copy_all_files_to_new_location(fruit_root +'\\Training\\Apple Red 1', work_root
+'\\Data\\Images','apple', data)

copy_all_files_to_new_location(fruit_root +'\\Test\\Apple Red 1', work_root
+'\\Data\\Images','apple', data)

# Get some orange files
copy_all_files_to_new_location(fruit_root +'\\Training\\Orange', work_root
+'\\Data\\Images','orange', data)

copy_all_files_to_new_location(fruit_root +'\\Test\\Orange', work_root
+'\\Data\\Images','orange', data)

# Create pandas dataframe with the data
meta = pd.DataFrame(data)

# Give names to the columns
meta.rename(columns={0:'basename',1:'classid'}, inplace=True)
meta.to_pickle(work_root+'\\Data\\meta.pkl')
\end{minted}

\subsection{Jupyter Notebook of the Test Project - Fruit Recognition} \label{app:jupyter}
\textit{see document fruit\_recognition.pdf}

%\includepdf[page={1-19}]{files/fruit_recognition.pdf}


\end{document}