import os
from shutil import copyfile
import pandas as pd

fruit_root = 'C:\\Users\\adam.true\\Desktop\\Fruit-Images-Dataset'
work_root = 'C:\\Users\\adam.true\\Desktop\\Work\\Amara_MachineLearning'

def get_files_in_path(mypath):
    f = []
    for (dirpath, dirnames, filenames) in os.walk(mypath):
        f.extend(filenames)
        break
    return f

def copy_file_to_new_location(oldFP, newFP):
    copyfile(oldFP, newFP)

def copy_all_files_to_new_location(oldFP, newFP, nameOfFruit, file_data):
    # Get all of the file names within the directory
    files_to_copy = get_files_in_path(oldFP)
    # We will copy the new file names into this data array along with the name of the fruit
    for f in files_to_copy:
        copy_file_to_new_location(oldFP+'\\'+f, newFP+'\\'+nameOfFruit+f)
        file_data.append([nameOfFruit+f, nameOfFruit])
    return file_data



data = []

# Get banana files
copy_all_files_to_new_location(fruit_root +'\\Training\\Banana', work_root +'\\Data\\Images','banana', data)
copy_all_files_to_new_location(fruit_root +'\\Test\\Banana', work_root +'\\Data\\Images','banana', data)

# Get some apple files
copy_all_files_to_new_location(fruit_root +'\\Training\\Apple Red 1', work_root +'\\Data\\Images','apple', data)
copy_all_files_to_new_location(fruit_root +'\\Test\\Apple Red 1', work_root +'\\Data\\Images','apple', data)

# Get some orange files
copy_all_files_to_new_location(fruit_root +'\\Training\\Orange', work_root +'\\Data\\Images','orange', data)
copy_all_files_to_new_location(fruit_root +'\\Test\\Orange', work_root +'\\Data\\Images','orange', data)

# Create pandas dataframe with the data
meta = pd.DataFrame(data)

# Give names to the columns
meta.rename(columns={0:'basename',1:'classid'}, inplace=True)
meta.to_pickle(work_root+'\\Data\\meta.pkl')
