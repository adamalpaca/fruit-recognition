# Fruit Recognition

This project is for the engineering faction of [University of Applied 
Sciences Western Switzerland, Valais//Wallis](https://www.hevs.ch/en/) in Sion. The idea was to create a very simple project to help students take their first steps in the complex world that is machine learning.

## Abstract

This is my first machine learning project. The idea is to use a [kNN classifier](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm) to identify fruits from a 100x100 pixel image.

Feature selection methods included : extract a histogram to reduce feature size and [principal component analysis](https://en.wikipedia.org/wiki/Principal_component_analysis) to reduce it even further.

This means the number of features went from 30'000 (100x100 pixels for each colour channel: red, green and blue) to 768 (3x256 colour values) for the histogram. Then we reduced it even further by using PCA to get down to 25 features.

The number of neighbours selected for the kNN was 70 but this should probably be reduced.   

The images of Fruits were taken from the data set [Fruits-360](https://github.com/Horea94/Fruit-Images-Dataset).


## Guide

The needed elements to complete this project are:

* Manual to follow the example : ``Documents/Machine_Learning_Tips.pdf``
* Jupyter notebook of the project : ``fruit_recognition.ipynb``
* Data used in the example, with serialisation file : ``fruits.zip``
* Python file for compiling the data into a zip : ``Utilities/data-serialisation.py``
* Master's course files (password protected) : ``Documents/Course_documents.zip``

## Results

The algorithm works with 100% accuracy on the test set. However when we try to use it with "real world" photographs, the performance drops drastically. This is due to the bias of the training data, in order to counter this we should probably preprocess the images and extract more varied features.

## Notes

These documents are redacted in UK English, so no complaining about the spelling of the word "colo**u**r". ;)
